
[1. Working with Request and Response Body] +++
   1. Spring MVC - HTTP Message Body handling using @RequestBody and @ResponseBody(bodyhandling)
   2. Spring MVC - Convert XML Message to Object using @RequestBody and @ResponseBody(convertxml)
   3. Spring MVC - Convert JSON Message to Object using @RequestBody and @ResponseBody(convertjson)
   4. Spring MVC - List of default registered HttpMessageConverter(msgconverter)
   5. Spring MVC - HttpMessageConverter Registration(registerconverter)
   6. Spring MVC - Creating a custom HttpMessageConverter(customconverter)
   7. Spring MVC - HttpMessageConverter for CSV conversion(csvconverter)
   8. Spring MVC - HttpMessageConverter for YAML conversion(ymlconverter)
   9. Spring MVC - Building RESTful Web Service using @RestController(restcontroller)
   10. Spring MVC - Using HttpEntity(httpentity)
   11. Spring MVC - Using RequestEntity and ResponseEntity(reqrespentity)
[2. Handling PUT Requests] +++
    1. Spring MVC - Handling HTTP PUT Request(handlingput)
    2. Spring MVC - Mapping XML body data of HTTP PUT request to Java Object(-)
    3. Spring MVC - Mapping JSON body data of HTTP PUT request to Java Object(putrest)
    4. Spring MVC - Unit Testing PUT Requests(testput)
[3. Handling PATCH Requests] +++

[12. Interceptor, Callable, Async Processing and Streaming(FebFlux and Project Reactor)] +

Spring MVC -> view -> (jsp, groovy, thymleaf) => html!
jsp, jsf ->  object, business model, MVC -> view -> Browsers ;-)

Mobile App -> adaptive, mobile application -> data

SomeSiteWithWeather-> html -> parse html -> changed design -> analytic ;-((((
WebServices -> not ui -> data manipulation
SOAP vs RESTful  vs gRPC vs GraphQL

REST based on HTTP -> representation state transfer(JSON, XML)

GET -> get resource
POST -> create new resource
PUT -> when need to update resource
DELETE -> when we need to delete
