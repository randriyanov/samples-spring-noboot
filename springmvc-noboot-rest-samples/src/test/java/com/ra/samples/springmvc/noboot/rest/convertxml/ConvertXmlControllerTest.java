package com.ra.samples.springmvc.noboot.rest.convertxml;

import com.ra.samples.springmvc.noboot.rest.MvcConfiguration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


@ExtendWith(SpringExtension.class)
@WebAppConfiguration
@ContextConfiguration(classes = MvcConfiguration.class)
public class ConvertXmlControllerTest {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup () {
        DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
        this.mockMvc = builder.build();
    }

    @Test
    public void testUserController () throws Exception {

        MockHttpServletRequestBuilder builder =
                MockMvcRequestBuilders.post("/convertxml/register")
                        .contentType(MediaType.APPLICATION_XML)
                        .content(createUserInXml(
                                "joe",
                                "joe@example.com",
                                "abc"));

        //create one moe user
        this.mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status()
                        .isCreated());

        builder = MockMvcRequestBuilders.post("/convertxml/register")
                .contentType(MediaType.APPLICATION_XML)
                .content(createUserInXml("mike",
                        "mike@example.com",
                        "123"));

        this.mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status()
                        .isCreated());

        //getting all users
        builder = MockMvcRequestBuilders.get("/convertxml")
                .accept(MediaType.APPLICATION_XML);
        this.mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status()
                        .isOk())
                .andDo(MockMvcResultHandlers.print());

    }

    private static String createUserInXml (String name, String email, String password) {
        return "<user>" +
                "<name>" + name + "</name>" +
                "<emailAddress>" + email + "</emailAddress>" +
                "<password>" + password + "</password>" +
                "</user>";
    }
}
