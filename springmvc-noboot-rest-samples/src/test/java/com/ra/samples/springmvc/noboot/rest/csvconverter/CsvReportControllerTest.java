package com.ra.samples.springmvc.noboot.rest.csvconverter;


import com.ra.samples.springmvc.noboot.rest.MvcConfiguration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;


@ExtendWith(SpringExtension.class)
@WebAppConfiguration
@ContextConfiguration(classes = MvcConfiguration.class)
public class CsvReportControllerTest {

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    @BeforeEach
    public void setup () {
        DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
        this.mockMvc = builder.build();
    }

    @Test
    public void testConsumerController () throws Exception {
        MockHttpServletRequestBuilder builder =
                MockMvcRequestBuilders.post("/csvconverter/newEmployee")
                        .contentType("text/csv")
                        .accept(MediaType.TEXT_PLAIN_VALUE)
                        .content(getNewEmployeeListInCsv());
        this.mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status()
                        .isOk())
                .andDo(MockMvcResultHandlers.print());
        ;
    }

    @Test
    public void testProducerController () throws Exception {
        MockHttpServletRequestBuilder builder =
                MockMvcRequestBuilders.get("/csvconverter/employeeList")
                        .accept("text/csv");
        this.mockMvc.perform(builder)
                .andExpect(MockMvcResultMatchers.status()
                        .isOk())
                .andDo(MockMvcResultHandlers.print());
    }

    public String getNewEmployeeListInCsv () {
        return "id, name, phoneNumber\n1,Joe,123-212-3233\n2,Sara,132-232-3111\n" +
                "3,Mike,111-222-3333\n";
    }
}
