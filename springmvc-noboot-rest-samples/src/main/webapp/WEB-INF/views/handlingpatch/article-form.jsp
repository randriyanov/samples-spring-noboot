<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>

<h3>Article Form</h3>
<form id="article-form">
    <pre>
         id: <input type="text" name="id" value="${article.id}" readonly>
    content: <input type="text" name="content" value="${article.content}">
                  <input type="submit" value="Submit">
    </pre>
</form>
<br/>
<script>
    $("#article-form").submit(function(event){
        event.preventDefault();
        var form = $(this);
        var id = form.find('input[name="id"]').val();
        var url = 'http://localhost:8080/handlingpatch/'+id;
        var content = form.find('input[name="content"]').val();
        $.ajax({
            type : 'PATCH',
            url : url,
            contentType: 'application/x-www-form-urlencoded',
            data : "id=" + id + "&content=" + content,
            success : function(data, status, xhr){
                //refresh the current page
                location.reload();
            },
            error: function(xhr, status, error){
                alert(error);
            }
        });
    });

</script>
</body>
</html>
