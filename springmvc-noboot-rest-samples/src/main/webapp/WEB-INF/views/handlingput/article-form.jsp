<html>
<head>
    <script
            src="https://code.jquery.com/jquery-3.4.1.min.js">
    </script>
</head>
<body>

<h3>Article Form</h3>
<form id="article-form">
    <pre>
         id: <input type="text" name="id">
    content: <input type="text" name="content">
                  <input type="submit" value="Submit">
    </pre>
</form>
<br/>
<div id="result"></div>

<script>
    $("#article-form").submit(function(event){
        event.preventDefault();
        var form = $(this);
        var id = form.find('input[name="id"]').val();
        var url = 'http://localhost:8080/handlingput/'+id;
        var content = form.find('input[name="content"]').val();

        $.ajax({
            type : 'PUT',
            url : url,
            contentType: 'application/x-www-form-urlencoded',
            data : "id="+id+"&content="+content,
            success : function(data, status, xhr){
                $("#result").html(data+
                    " link: <a href='"+url+"'>"+url+"</a>");
            },
            error: function(xhr, status, error){
                console.log(error);
                alert(error);
            }
        });
    });

</script>
</body>
</html>