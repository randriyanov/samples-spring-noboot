<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>

<h3>HTTP PUT request with JSON Body Example</h3>
<form id="article-form">
    <pre>
         id: <input type="text" name="id">
    content: <input type="text" name="content">
                  <input type="submit" value="Submit">
    </pre>
</form>
<br/>
<div id="result"></div>

<script>
    $("#article-form").submit(function(event){
        event.preventDefault();
        var form = $(this);
        var idVal = form.find('input[name="id"]').val();
        var contentVal = form.find('input[name="content"]').val();
        var url = 'http://localhost:8080/putrest/'+idVal;
        var jsonString = JSON.stringify({id: idVal, content: contentVal});
        console.log(jsonString);
        $.ajax({
            type : 'PUT',
            url : url,
            contentType: 'application/json',
            data : jsonString,
            success : function(data, status, xhr){
                $("#result").html(data+
                    " link: <a href='"+url+"'>"+url+"</a>");
            },
            error: function(xhr, status, error){
                alert(error);
            }
        });
    });
</script>
</body>
</html>