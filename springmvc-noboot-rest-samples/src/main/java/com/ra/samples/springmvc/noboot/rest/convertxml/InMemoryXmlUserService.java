package com.ra.samples.springmvc.noboot.rest.convertxml;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
public class InMemoryXmlUserService implements XmlUserService {
    private Map<Long, XmlUser> userMap = new LinkedHashMap<>();

    @Override
    public void saveUser (XmlUser user) {
        if (user.getId() == null) {
            user.setId((long) (userMap.size() + 1));
        }
        userMap.put(user.getId(), user);

    }

    @Override
    public List<XmlUser> getAllUsers () {
        return new ArrayList<>(userMap.values());
    }
}
