package com.ra.samples.springmvc.noboot.rest.csvconverter;

import java.util.List;

public class CsvListParam <T> {

    private List<T> list;

    public List<T> getList () {
        return list;
    }

    public void setList (List<T> list) {
        this.list = list;
    }
}
