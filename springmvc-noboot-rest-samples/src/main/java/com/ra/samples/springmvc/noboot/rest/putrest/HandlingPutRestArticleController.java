package com.ra.samples.springmvc.noboot.rest.putrest;

import com.ra.samples.springmvc.noboot.rest.handlingput.Article;
import com.ra.samples.springmvc.noboot.rest.handlingput.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/putrest")
public class HandlingPutRestArticleController {

    @Autowired
    private ArticleService articleService;

    @GetMapping
    public String getArticleForm() {
        return "putrest/article-form";
    }

    @PutMapping("/{id}")
    @ResponseBody
    public String createNewArticle(@RequestBody Article article) {
        articleService.saveArticle(article);
        return "Article created.";
    }

    @GetMapping("/{id}")
    public String getArticle(@PathVariable("id") long id, Model model) {
        Article article = articleService.getArticleById(id);
        model.addAttribute("article", article);
        return "putrest/article-page";
    }
}
