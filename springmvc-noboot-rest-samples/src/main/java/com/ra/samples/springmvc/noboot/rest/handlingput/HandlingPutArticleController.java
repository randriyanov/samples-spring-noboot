package com.ra.samples.springmvc.noboot.rest.handlingput;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/handlingput")
public class HandlingPutArticleController {

    @Autowired
    private ArticleService articleService;

    @GetMapping
    public String getArticleForm() {
        return "handlingput/article-form";
    }

    @PutMapping("/{id}")
    @ResponseBody
    public String createNewArticle(@RequestBody MultiValueMap<String, String> formParams) {
        System.out.println(formParams);
        long id = Long.parseLong(formParams.getFirst("id"));
        String content = formParams.getFirst("content");
        Article article = new Article(id, content);
        articleService.saveArticle(article);
        return "Article created.";
    }

    @GetMapping("/{id}")
    public String getArticle(@PathVariable("id") long id, Model model) {
        Article article = articleService.getArticleById(id);
        model.addAttribute("article", article);
        return "handlingput/article-page";
    }
}
