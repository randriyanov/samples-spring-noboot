package com.ra.samples.springmvc.noboot.rest.convertjson;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("jsonuser")
public class JsonUserController {

    @Autowired
    private JsonUserService userService;

    @RequestMapping(value = "register", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void handleJsonPostRequest(@RequestBody @Valid JsonUser user, Model model) {
        System.out.println("saving user: " + user);
        userService.saveUser(user);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public List<JsonUser> handleAllUserRequest() {
        return userService.getAllUsers();
    }
}
