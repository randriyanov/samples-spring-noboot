package com.ra.samples.springmvc.noboot.rest.convertjson;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
public class InMemoryJsonUserService implements JsonUserService {
    private Map<Long, JsonUser> userMap = new LinkedHashMap<>();

    @Override
    public void saveUser(JsonUser user) {
        if (user.getId() == null) {
            user.setId((long) (userMap.size() + 1));
        }
        userMap.put(user.getId(), user);

    }

    @Override
    public List<JsonUser> getAllUsers() {
        return new ArrayList<>(userMap.values());
    }
}