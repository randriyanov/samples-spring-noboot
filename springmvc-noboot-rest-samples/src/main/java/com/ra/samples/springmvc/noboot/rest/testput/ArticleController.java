package com.ra.samples.springmvc.noboot.rest.testput;

import com.ra.samples.springmvc.noboot.rest.handlingput.Article;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/testput")
public class ArticleController {
    //for xml and json
    @PutMapping("/{id}")
    @ResponseBody
    public String createNewArticle(@RequestBody Article article) {
        System.out.println("Creating Article in controller: " + article);
        return "Article created.";
    }

    //for x-www-form-urlencoded
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @ResponseBody
    public String createNewArticleWithFormParams(@RequestBody MultiValueMap<String, String> formParams) {
        System.out.println("Creating Article in controller. MultiValueMap= " + formParams);
        return "Article created.";
    }
}
