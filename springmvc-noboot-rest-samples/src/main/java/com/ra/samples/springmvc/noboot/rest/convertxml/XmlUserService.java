package com.ra.samples.springmvc.noboot.rest.convertxml;

import java.util.List;

public interface XmlUserService {

    void saveUser(XmlUser user);

    List<XmlUser> getAllUsers();
}
