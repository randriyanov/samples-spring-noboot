package com.ra.samples.springmvc.noboot.rest.convertxml;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class UserList {

    private List<XmlUser> users;

    public List<XmlUser> getUsers () {
        return users;
    }

    public void setUsers (List<XmlUser> users) {
        this.users = users;
    }
}
