package com.ra.samples.springmvc.noboot.rest.handlingpatch;

import com.ra.samples.springmvc.noboot.rest.handlingput.Article;
import com.ra.samples.springmvc.noboot.rest.handlingput.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

/**
 * PUT vs PATCH vs POST
 * PUT method creates/replaces the resource at the requested URI.
 *
 * PATCH method modifies the existing resource (partially) at the requested URI.
 *
 * POST method creates/modifies the resource without targeting an URI.
 * After modification, how user can make use of the same resource,
 * that's entirely dependent on the web application logic.
 */
@Controller
@RequestMapping("/handlingpatch")
public class HandlingPatchArticleController {

    @Autowired
    private ArticleService articleService;

    @PatchMapping("/{id}")
    @ResponseBody
    public String patchArticle(@RequestBody MultiValueMap<String, String> formParams) {
        System.out.println(formParams);
        long id = Long.parseLong(formParams.getFirst("id"));
        String content = formParams.getFirst("content");
        articleService.updateArticle(id, content);
        return "Article updated.";
    }

    @GetMapping("/{id}")
    public String getArticle(@PathVariable("id") long id, Model model) {
        Article article = articleService.getArticleById(id);
        model.addAttribute("article", article);
        return "handlingpatch/article-form";
    }
}
