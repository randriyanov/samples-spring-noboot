package com.ra.samples.springmvc.noboot.rest.reqrespentity;

import com.ra.samples.springmvc.noboot.rest.convertjson.JsonUser;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

/**
 *
 * HttpEntity<T> is a helper object which encapsulates header and body of an HTTP request or response.
 * It can be used as a handler method parameter.
 *
 * To get header and body information,the following methods are used:
 *
 * public HttpHeaders getHeaders()
 * public T getBody()
 * The usage of HttpEntity is an alternative to using the two parameter:
 * @RequestHeader HttpHeaders
 * and
 * @RequestBody String/backing type
 *
 * HttpEntity can be used to return response as well.
 * The additional advantage in this case, when comparing with @ResponseBody is, it can include the headers in the response as well.
 */
@Controller
@RequestMapping("reqrespentity")
public class RequestResponseController {

    @RequestMapping
    public HttpEntity<String> handleRequest (HttpEntity<String> requestEntity) {
        System.out.println("request body: " + requestEntity.getBody());
        HttpHeaders headers = requestEntity.getHeaders();
        System.out.println("request headers " + headers);
        HttpEntity<String> responseEntity = new HttpEntity<String>("my response body");
        return responseEntity;
    }

    @RequestMapping("/user")
    public HttpEntity<String> handleUserRequest (HttpEntity<JsonUser> requestEntity) {
        JsonUser user = requestEntity.getBody();
        System.out.println("request body: " + user);
        System.out.println("request headers " + requestEntity.getHeaders());

        MultiValueMap<String, String> headers = new HttpHeaders();
        headers.put("Cache-Control", Arrays.asList("max-age=3600"));

        HttpEntity<String> responseEntity = new HttpEntity<>("my response body", headers);
        return responseEntity;
    }
}
