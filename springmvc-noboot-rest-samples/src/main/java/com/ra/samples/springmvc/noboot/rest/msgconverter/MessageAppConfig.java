package com.ra.samples.springmvc.noboot.rest.msgconverter;

import org.springframework.stereotype.Component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;

@Component
public class MessageAppConfig {

    @Autowired
    private RequestMappingHandlerAdapter handlerAdapter;

    @EventListener
    public void handleContextRefresh(ContextRefreshedEvent event) {
        System.out.println("-- context refreshed --");
        System.out.println("context: "+
                event.getApplicationContext());

        handlerAdapter.getMessageConverters().forEach(System.out::println);
        System.out.println("-------");
    }
}
