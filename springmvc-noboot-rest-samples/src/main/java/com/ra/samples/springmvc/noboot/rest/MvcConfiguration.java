package com.ra.samples.springmvc.noboot.rest;

import com.ra.samples.springmvc.noboot.rest.csvconverter.CsvHttpMessageConverter;
import com.ra.samples.springmvc.noboot.rest.customconverter.CustomReportConverter;
import com.ra.samples.springmvc.noboot.rest.registerconverter.TheCustomConverter;
import com.ra.samples.springmvc.noboot.rest.ymlconverter.YamlHttpMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import java.util.List;

@EnableWebMvc
@Configuration
@ComponentScan("com.ra.samples.springmvc.noboot.rest")
public class MvcConfiguration implements WebMvcConfigurer {

    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(new TheCustomConverter());
        converters.add(new CustomReportConverter());
        converters.add(new CsvHttpMessageConverter<>());
        converters.add(new YamlHttpMessageConverter<>());
    }

    @Bean
    public ViewResolver viewResolver () {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");
        return viewResolver;
    }
}
