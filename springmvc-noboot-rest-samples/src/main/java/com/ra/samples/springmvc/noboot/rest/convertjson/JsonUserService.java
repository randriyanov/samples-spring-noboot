package com.ra.samples.springmvc.noboot.rest.convertjson;

import java.util.List;

public interface JsonUserService {

    void saveUser(JsonUser user);

    List<JsonUser> getAllUsers();
}
