https://www.logicbig.com/tutorials/spring-framework/spring-web-mvc.html
Spring WEB MVC
1. Understanding MVC configuration(init web context) +++
2. Unit Testing +++
[3. Implementing Controllers]++
 1. Spring MVC - Mapping Requests with @RequestMapping(reqmapping)
 2. Spring MVC - Binding URI template variable with @PathVariable(pathvariable)
 3. Spring MVC - URI Patterns(uri)
 4. Spring MVC - Binding URL query parameters with @RequestParam(reqparam)
 5. Spring MVC - Binding HTTP request header attributes with @RequestHeader(reqheader)
 6. Spring MVC - Automatic Parameter Names Discovery(namediscover)
 7. Spring MVC - Binding Request Parameters and Path Variables to Java Backing Objects(bindpathvar)
 8. Spring MVC - Binding Java Backing Objects with Custom Converters(bindobject)
 9. Spring MVC - Cookie handling(cookie)
 10. Spring MVC - Using java.util.Optional as handler method parameter(opthandler)
 11. Spring MVC - Mapping Multi Valued Query Parameter with Arrays(multiparam)
 12. Spring MVC - Mapping Query Parameters To Enum(enumparam)
 13. Spring MVC - Composed @RequestMapping Variants(composed)
 14. Spring MVC - Built-in support for HTTP HEAD and HTTP OPTIONS(headoption)
 15. Spring MVC - Meta Annotations (meta)
 16. Spring MVC - Listening to HTTP request as events(event)
[4. HTTP Session Handling]++
  1. Spring MVC - Using Session Scoped Beans(sessionscope)
  2. Spring MVC - Using HttpSession as Controller Method Argument(httpsession)
[5. Web Aware Bean Scopes]++
  1. Spring MVC - Request Scoped Bean(reqscope) X
  2. Spring MVC - Session Scoped Bean created via Class Based Proxy Example(reqproxy)
  3. Spring MVC - Application Scoped Bean Example(appscope)
[6. ModelAttribute, SessionAttribute and RequestAttribute]+++
  1. Spring MVC - Prepopulate Model with common attributes using @ModelAttribute(modelattr)
  2. Spring MVC - Providing a Model attribute value to a handler method by using @ModelAttribute(modelattrhandler)
  3. Spring MVC - Store Model attributes in HTTP session with @SessionAttributes(sessionattr)
  4. Spring MVC - Accessing pre-existing request attributes using @RequestAttribute(reqattribute)
  5. Spring MVC - Accessing pre-existing session attributes using @SessionAttribute(presessionatr)
[7. Form Submission, Handling and Validation] +++
  1. Spring MVC - Simple Form Submission(simpleform)
  2. Spring MVC - Form Validation with JSR-349 Bean Validation(validform)
  3. Spring MVC - Form Validation using Bean Validation and Spring JSP Form Tag(messagebeanvalid)
  4. Spring MVC - Form Validation using Java Bean Validation API and External Message Source(messagebeanvalid)
  5. Spring MVC - Form Validation using Spring Validator Interface(validintrface)
[8. Data Binding and Formatting] ++
  1. Spring MVC - Customizing data binding with @InitBinder(validintrface)
  2. Spring MVC - Customizing existing formatters using @InitBinder(formatting)
  3. Spring MVC - Annotation-driven Formatting(annotformatter)
  4. Spring MVC - Creating a new Formatter(newformatter)
  5. Spring MVC - Creating a new Custom Formatter Annotation
  6. Spring MVC - Custom Data binding with @RequestParam example(paramformatter)
  7. Spring MVC - Customizing ConfigurableWebBindingInitializer(bindintializer)
[9. @ControllerAdvice] ++
  1. Spring MVC - @ControllerAdvice With @ModelAttribute Example
[10. File Upload] +
//REST in module
Working with Request and Response Body +++
Handling PUT Requests +++
Handling PATCH Requests +++
//
[11. Redirecting and Forwarding] ++
    1. Spring MVC - Redirecting using RedirectView
    2. Spring MVC - Passing Data To the Redirect Target
    3. Spring MVC - Using redirect: prefix
    4. Spring MVC - Using forward: prefix
    5. Spring MVC - Using RedirectAttributes
    6. Spring MVC - Prevent duplicate form submission
[12. Interceptor, Callable, Async Processing and Streaming(FebFlux and Project Reactor)] +
[13. Profiles] +
[14. Working with Locale, i18n and Time Zone] +
15. Themes +
16. Implicit Attribute Handling +
[17. Exception Handling] +++
    1. Spring MVC - Handling exceptions using HandlerExceptionResolver
    2. Spring MVC - Exception to view mapping using SimpleMappingExceptionResolver
    3. Spring MVC - Annotation based Exception handling using @ExceptionHandler(exceptionhandler)
    4. Spring MVC - Understanding Spring internal MVC Exceptions
    5. Spring MVC - Creating a custom HandlerExceptionResolver
    6. Spring MVC - Using multiple HandlerExceptionResolvers together
    7. Spring MVC - Ordering and customization of default HandlerExceptionResolvers
    (https://www.logicbig.com/tutorials/spring-framework/spring-web-mvc/default-exception-resolvers.html)
[18. Standard Headers Support] ++
19. HandlerMapping and HandlerAdapter +
[20. View Controllers] +++
    1. Spring MVC - Mapping a URL to a view without a controller(mvcconfig)
    2. 
21. Using Servlet Components in Spring MVC +
[22. Content Negotiation] +++
    1. Spring MVC - Content Negotiation
    2. Spring MVC - Path Extension Content Negotiation Strategy
    3. Spring MVC - Parameter Content Negotiation Strategy
    4. Spring MVC - Setting Default Content Type with ContentNegotiationConfigurer
    5. Spring MVC - Using a Custom ContentNegotiationStrategy
23. Spring Views +++ (thymeleaf)
