package com.ra.samples.springmvc.noboot.reqmapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/users")
public class UserControllerParams {
    //PathVariable and RequestParams
    //PathVariable -> /users/1
    //RequestParams -> /users?id=4
    @RequestMapping(params = "id=4")
    public String handleUserId4() {
        System.out.println("got param id = 4");
        return "";
    }

    @RequestMapping(params = "id=10")
    public String handleUserId10() {
        System.out.println("got param id = 10");
        return "";
    }
}
