package com.ra.samples.springmvc.noboot.sessionattr;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;

import javax.servlet.http.HttpServletRequest;

@Controller
@SessionAttributes("visitor")
@RequestMapping("sessionattr")
public class SessionAttrTradeController {

    @RequestMapping("/trades/**")
    public String handleRequestById (@ModelAttribute("visitor") SessionAttrVisitor visitor, Model model,
                                     HttpServletRequest request, SessionStatus sessionStatus) {

        model.addAttribute("msg", "trades request, serving page " + request.getRequestURI());
        if (request.getRequestURI()
                .endsWith("clear")) {
            sessionStatus.setComplete();
            return "clear-page";
        } else {
            visitor.addPageVisited(request.getRequestURI());
            return "session-trades";
        }
    }



    @ModelAttribute("visitor")
    public SessionAttrVisitor getVisitor (HttpServletRequest request) {
        return new SessionAttrVisitor(request.getRemoteAddr());
    }
}
//FIRST Call
//1. we call sessionattr/trades/1
//2. first call on servlet level no session!!(request.getSession == null)
//3. On first call we get into method marked with @ModelAttribute -> insert result into session
//4. @SessionAttributes("visitor") spring add this attribute into session
//5. Controller handler @ModelAttribute("visitor") -> inject value in arguments
//Second Call
//1. we call sessionattr/trades/1
//2. second call on servlet level and found session!!!
//3. @SessionAttributes("visitor") -> handler