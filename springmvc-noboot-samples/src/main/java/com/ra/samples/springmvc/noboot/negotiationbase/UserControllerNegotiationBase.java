package com.ra.samples.springmvc.noboot.negotiationbase;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

//http://localhost:8080/negotiationbase?id=2
//http://localhost:8080/user?id=2&format=xml
@Controller
@RequestMapping("negotiationbase")
public class UserControllerNegotiationBase {

    @RequestMapping(produces = MediaType.APPLICATION_XML_VALUE)
    @ResponseBody
    public UserNegotiation getUserById (@RequestParam("id") long userId) {
        //creating dummy user
        UserNegotiation user = new UserNegotiation();
        user.setId(userId);
        user.setName("joe");
        user.setEmailAddress("joe@example.com");
        return user;
    }

    @RequestMapping
    @ResponseBody
    public String getUserStringById (@RequestParam("id") long userId) {
        return "joe, id: "+userId;
    }
}
