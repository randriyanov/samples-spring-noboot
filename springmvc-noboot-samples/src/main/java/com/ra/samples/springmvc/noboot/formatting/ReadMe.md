Formatters specify the data format to be rendered in user interface. 
They also provide a way to convert string input from user interface to 
Java data type.
Converter are used to convert one type to another.
Spring built-in Formatters: DateFormatter, NumberStyleFormatter and CurrencyStyleFormatter.