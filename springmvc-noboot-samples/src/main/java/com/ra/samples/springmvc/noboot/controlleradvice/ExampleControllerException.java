package com.ra.samples.springmvc.noboot.controlleradvice;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/exception")
public class ExampleControllerException {

    @RequestMapping("/test1")
    public String handleRequest1 () throws Exception {
        String msg = String.format("Test exception from class: %s",
                this.getClass().getSimpleName());

        throw new Exception(msg);
    }

    @RequestMapping("/test2")
    public String handleRequest2 () throws Exception {
        String msg = String.format("Test exception from class: %s",
                this.getClass().getSimpleName());

        throw new IllegalArgumentException(msg);
    }
}
