package com.ra.samples.springmvc.noboot.controlleradvice;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.LongAdder;


/**
 * @ControllerAdvice is a specialization of @Component
 * which can be used to define methods with @ExceptionHandler, @InitBinder,
 * and @ModelAttribute annotations. Such methods are applied
 * to all @RequestMapping methods across multiple @Controller classes instead
 * of just local @Controller class.
 *
 * Following example shows how to use @ModelAttribute in @ControllerAdvice class
 * to globally apply module attributes.
 */

@ControllerAdvice
public class PageCountersControllerAdvice {

    private ConcurrentHashMap<String, LongAdder> counterMap = new ConcurrentHashMap<>();

    @ModelAttribute
    public void handleRequest(HttpServletRequest request, Model model) {
        String requestURI = request.getRequestURI();
        //counter increment for each access to a particular uri
        counterMap.computeIfAbsent(requestURI, key -> new LongAdder())
                .increment();
        //populating counter in the model
        model.addAttribute("counter", counterMap.get(requestURI).sum());
        //populating request URI in the model
        model.addAttribute("uri", requestURI);
    }
}
