package com.ra.samples.springmvc.noboot.appscope;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;


import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("first")
public class FirstConfigController {

    @Autowired
    private AppLevelPreference appLevelPreference;

    @GetMapping("/**")
    public String appHandler(Model model, HttpServletRequest req) {
        model.addAttribute("pref", appLevelPreference);
        model.addAttribute("uri", req.getRequestURI());
        model.addAttribute("msg", "response from app1");
        return "app-page-config";
    }

    @PostMapping("/**")
    public String appPostHandler(String fontSize, String background, HttpServletRequest req) {
        appLevelPreference.setBackground(background);
        appLevelPreference.setFontSize(fontSize);
        System.out.println(req.getRequestURI());
        return "redirect:" + req.getRequestURI();//redirect to GET
    }
}
