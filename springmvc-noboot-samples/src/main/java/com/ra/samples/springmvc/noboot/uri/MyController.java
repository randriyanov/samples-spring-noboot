package com.ra.samples.springmvc.noboot.uri;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;

@Controller
@ResponseBody
public class MyController {

    @RequestMapping("/car?/s?o?/info")//score 0
    public String test1(HttpServletRequest request) {
        return "from test1(), request uri: " + request.getRequestURI();
    }

    @RequestMapping("/c*/s*d/info")//score 2, length = 12
    public String test2(HttpServletRequest request) {
        return "from test2(), request uri: " + request.getRequestURI();
    }

    @RequestMapping("/card/**")//score 2 but will be used after others because of prefix pattern
    public String test3(HttpServletRequest request) {
        return "from test3(), request uri: " + request.getRequestURI();
    }

    @RequestMapping("/card/{type}/{id:i.*}")//2 template variables + 1 wildcard = score 3
    public String test4(@PathVariable String type, @PathVariable String id,
                        HttpServletRequest request) {
        return "from test4(), request uri: " + request.getRequestURI() + "\n" +
                "type: " + type + ", id: " + id;
    }

    @RequestMapping("/card/{type}/{id:i.+}")//score 2, length 9 (/card/#/#)
    public String test5(@PathVariable String type, @PathVariable String id,
                        HttpServletRequest request) {
        return "from test5(), request uri: " + request.getRequestURI() + "\n" +
                "type: " + type + ", id: " + id;
    }
}