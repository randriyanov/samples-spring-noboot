package com.ra.samples.springmvc.noboot.formatting;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Date;

public class BasicFormatterTrade {

    private long tradeId;
    private String buySell;
    private Currency buyCurrency;
    private Currency sellCurrency;
    private BigDecimal amount;
    private Date tradeDate;

    public long getTradeId () {
        return tradeId;
    }

    public void setTradeId (long tradeId) {
        this.tradeId = tradeId;
    }

    public String getBuySell () {
        return buySell;
    }

    public void setBuySell (String buySell) {
        this.buySell = buySell;
    }

    public Currency getBuyCurrency () {
        return buyCurrency;
    }

    public void setBuyCurrency (Currency buyCurrency) {
        this.buyCurrency = buyCurrency;
    }

    public Currency getSellCurrency () {
        return sellCurrency;
    }

    public void setSellCurrency (Currency sellCurrency) {
        this.sellCurrency = sellCurrency;
    }

    public BigDecimal getAmount () {
        return amount;
    }

    public void setAmount (BigDecimal amount) {
        this.amount = amount;
    }

    public Date getTradeDate () {
        return tradeDate;
    }

    public void setTradeDate (Date tradeDate) {
        this.tradeDate = tradeDate;
    }

    @Override
    public String toString () {
        return "Trade{" +
                "tradeId=" + tradeId +
                ", buySell='" + buySell + '\'' +
                ", buyCurrency='" + buyCurrency + '\'' +
                ", sellCurrency='" + sellCurrency + '\'' +
                '}';
    }
}
