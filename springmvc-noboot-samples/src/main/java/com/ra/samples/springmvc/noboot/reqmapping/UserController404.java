package com.ra.samples.springmvc.noboot.reqmapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = "/not/found/users")
public class UserController404 {

    //GET
    @RequestMapping
    public String handleAllUsersRequest(){
        return "all-users";
    }

}
