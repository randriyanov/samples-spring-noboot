package com.ra.samples.springmvc.noboot.appscope;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

/**
 * A single instance of 'application' scoped bean lives within a ServletContext instance.
 * That means it can be used between multiple servlet based applications running in the same ServletContext,
 * e.g. two Spring's ApplicationContexts can use the same 'application' scoped bean.
 *
 * The default 'singleton' bean lives only within a single ApplicationContext,
 * whereas, an 'application' bean lives within ServletContext i.e. across multiple ApplicationContexts.
 * Spring stores 'application' scoped bean as a regular ServletContext attribute.
 */
public class SecondWebInitializer implements WebApplicationInitializer {

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        registerSpringContext(servletContext, FirstWebConfig.class, "dispatcher1", "/app1/*");//first config
        registerSpringContext(servletContext, SecondWebConfig.class, "dispatcher2", "/app2/*");//second config
    }

    private void registerSpringContext(ServletContext servletContext, Class<?> configClass,
                                       String servletName, String mapping) {
        AnnotationConfigWebApplicationContext ctx =
                new AnnotationConfigWebApplicationContext();
        ctx.register(configClass);
        ctx.setServletContext(servletContext);

        ServletRegistration.Dynamic servlet =
                servletContext.addServlet(servletName, new DispatcherServlet(ctx));
        servlet.setLoadOnStartup(1);
        servlet.addMapping(mapping);
    }
}
