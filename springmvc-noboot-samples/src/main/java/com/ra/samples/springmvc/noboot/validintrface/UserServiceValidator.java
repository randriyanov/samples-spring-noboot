package com.ra.samples.springmvc.noboot.validintrface;

import java.util.List;

public interface UserServiceValidator {

    void saveUser(UserValidatorModel user);

    List<UserValidatorModel> getAllUsers();
}
