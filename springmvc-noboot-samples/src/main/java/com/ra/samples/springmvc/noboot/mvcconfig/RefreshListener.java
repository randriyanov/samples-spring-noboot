package com.ra.samples.springmvc.noboot.mvcconfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;

@Component
public class RefreshListener {

    @Autowired
    private RequestMappingHandlerAdapter handlerAdapter;

    @EventListener
    public void handleContextRefresh(ContextRefreshedEvent event) {
        System.out.println("-- Context Listener Refresh --");
        handlerAdapter.getMessageConverters().stream()
                .map(c -> c.toString())
                .forEach(System.out::println);
    }
}
