package com.ra.samples.springmvc.noboot.sessionscope;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.inject.Provider;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("sessionscope/trades")
public class SessionScopeTradeController {

    @Autowired
    private Provider<Visitor> visitorProvider;

    @RequestMapping("/**")
    public String handleRequestById (Model model, HttpServletRequest request) {
        model.addAttribute("msg", "trades request, serving page " + request.getRequestURI());
        System.out.println(request.getSession().getId());
        visitorProvider.get()
                .addPageVisited(request.getRequestURI());
        return "session-traders-page";
    }
}
