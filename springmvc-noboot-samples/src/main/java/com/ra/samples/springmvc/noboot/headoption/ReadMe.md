@RequestMapping methods mapped to "GET" 
are also implicitly mapped to "HEAD", i.e. there is no need to have "HEAD" explicitly declared. 
An HTTP HEAD request is processed as if it were an HTTP GET except 
instead of writing the body only the number of bytes are counted and the "Content-Length" header set.