package com.ra.samples.springmvc.noboot.uri;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/another/")
@ResponseBody
public class MyController3 {


    @RequestMapping("/c*/s*d/info")//score 2, length = 12, wildcards=2
    public String anotherTest1(HttpServletRequest request) {
        return "from anotherTest1(), request uri: " + request.getRequestURI();
    }


    @RequestMapping("/card/{type}/inf{id:.+}")//score 2, length 12 (/card/#/info), wildcard = 0
    public String anotherTest2(@PathVariable String type, @PathVariable String id,
                               HttpServletRequest request) {
        return "from anotherTest2(), request uri: " + request.getRequestURI()+"\n"+
                "type: "+type+", id: "+id;
    }
}
