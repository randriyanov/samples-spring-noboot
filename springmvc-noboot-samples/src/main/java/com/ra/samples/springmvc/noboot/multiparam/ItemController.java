package com.ra.samples.springmvc.noboot.multiparam;

import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
public class ItemController {

    @RequestMapping("/items")
    public String handleRequest(@RequestParam("id") String[] itemIds) {
        StringBuilder response = new StringBuilder();
        for (String itemId : itemIds) {
            response.append("item with string id ").append(itemId).append("<br/>");
        }
        return response.toString();
    }

    @RequestMapping("/items2")
    public String handleRequest2(@RequestParam("id") int[] itemIds) {
        StringBuilder response = new StringBuilder();
        for (int itemId : itemIds) {
            response.append("item with int id ").append(itemId).append("<br/>");
        }
        return response.toString();
    }

    @RequestMapping("/items3")
    public String handleRequest3(@RequestParam MultiValueMap<String, String> queryMap) {
        StringBuilder response = new StringBuilder();
        List<String> itemIds = queryMap.get("id");
        for (String itemId : itemIds) {
            response.append("item from map with String id ").append(itemId).append("<br/>");
        }
        return response.toString();
    }
}
