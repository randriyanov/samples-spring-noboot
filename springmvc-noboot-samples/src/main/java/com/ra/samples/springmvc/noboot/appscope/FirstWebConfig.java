package com.ra.samples.springmvc.noboot.appscope;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;


@EnableWebMvc
@Configuration
public class FirstWebConfig {

    @Bean
    public FirstConfigController firstConfigController() {
        return new FirstConfigController();
    }

    @Bean
    @Scope(value = WebApplicationContext.SCOPE_APPLICATION)
    public AppLevelPreference pref() {
        return new AppLevelPreference();
    }

    @Bean
    public ViewResolver viewResolver() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");
        return viewResolver;
    }
}
