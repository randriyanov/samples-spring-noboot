package com.ra.samples.springmvc.noboot.restcontroller;

public class MessageResult {
    private String errorCode;
    private String description;

    public MessageResult(String errorCode, String description) {
        this.errorCode = errorCode;
        this.description = description;
    }

    public MessageResult() {
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
