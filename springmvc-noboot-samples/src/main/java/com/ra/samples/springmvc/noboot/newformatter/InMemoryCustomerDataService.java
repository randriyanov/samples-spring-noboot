package com.ra.samples.springmvc.noboot.newformatter;

import org.fluttercode.datafactory.impl.DataFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Stream;

@Service
public class InMemoryCustomerDataService implements CustomerDataService {

    private Map<Long, Customer> customerMap = new HashMap<>();


    @PostConstruct
    private void postConstruct () {
        Stream.iterate(1L, a -> a + 1)
                .limit(10)
                .forEach(this::createCustomer);
    }

    private void createCustomer (Long id) {
        DataFactory df = new DataFactory();
        Customer c = new Customer();
        c.setName(df.getName());
        c.setId(id);
        NewFormatterAddress a = new NewFormatterAddress();
        a.setStreet(df.getNumberText(4) + " " + df.getStreetName());
        a.setCity(df.getCity());
        a.setCounty(df.getItem(Locale.getISOCountries()));
        a.setZipCode(df.getNumberText(5));
        c.setAddress(a);
        customerMap.put(c.getId(), c);
    }



    @Override
    public List<Customer> getAllCustomers () {
        return new ArrayList<>(customerMap.values());
    }
}
