package com.ra.samples.springmvc.noboot.bindobject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

@Service
public class TradeIdToTradeConverter implements Converter<String, Trade> {

    private TradeService tradeService;

    @Autowired
    public TradeIdToTradeConverter(TradeService tradeService) {
        this.tradeService = tradeService;
    }

    @Override
    public Trade convert(String id) {
        try {
            Long tradeId = Long.valueOf(id);
            return tradeService.getTradeById(tradeId);
        } catch (NumberFormatException e) {
            return null;
        }
    }

}
