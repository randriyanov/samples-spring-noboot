package com.ra.samples.springmvc.noboot.simpleform;

public interface UserService {

    void saveUser(User user);
}
