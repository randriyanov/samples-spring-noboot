package com.ra.samples.springmvc.noboot.modelattrhandler;

import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.stream.IntStream;

@Service
public class ModelAttributeUserServiceImpl implements ModelAttributeUserService{

    private Map<Long, ModelAttributeUser> userMap = new HashMap<>();

    @PostConstruct
    private void postConstruct(){
        Random random = new Random();
        List<String> roles = Arrays.asList("admin", "real-only", "guest");
        for (int i = 1; i <= 10; i++) {
            ModelAttributeUser user = new ModelAttributeUser();
            user.setId(i);
            user.setName(UUID.randomUUID().toString() + " Name");
            user.setRole(roles.get(random.nextInt(3)));
            userMap.put((long) i, user);
        }
    }

    public ModelAttributeUser getUserById(long id){
        return userMap.get(id);
    }
}
