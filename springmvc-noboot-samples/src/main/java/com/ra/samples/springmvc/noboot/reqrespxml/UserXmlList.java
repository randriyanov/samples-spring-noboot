package com.ra.samples.springmvc.noboot.reqrespxml;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
public class UserXmlList {
    private List<UserXml> users;

    public List<UserXml> getUsers () {
        return users;
    }

    public void setUsers (List<UserXml> users) {
        this.users = users;
    }
}
