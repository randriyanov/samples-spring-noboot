package com.ra.samples.springmvc.noboot.newformatter;

import java.util.List;

public interface CustomerDataService {

    List<Customer> getAllCustomers();
}
