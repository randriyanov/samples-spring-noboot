package com.ra.samples.springmvc.noboot.validintrface;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@RequestMapping("/validintrface")
public class UserValidatorController {

    @Autowired
    private UserServiceValidator userService;

    @Autowired
    private UserValidatorImpl userValidator;

    /**
     * To customize request parameter data binding,
     * we can use @InitBinder annotated methods within our controller.
     *
     * This customization is not limited to request parameters,
     * it can be applied to template URI variables and backing/command objects.
     * @param binder
     */
    @InitBinder("user")
    public void customizeBinding (WebDataBinder binder) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        dateFormatter.setLenient(false);
        binder.registerCustomEditor(Date.class, "dateOfBirth",
                new CustomDateEditor(dateFormatter, true));
        binder.setValidator(userValidator);
    }

    @RequestMapping(method = RequestMethod.GET)
    public String handleGetRequest (Model model) {
        model.addAttribute("user", new UserValidatorModel());
        return "user-registration-valid-interface";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String handlePostRequest (@Valid @ModelAttribute("user") UserValidatorModel user,
                                     BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "user-registration-valid-interface";
        }

        userService.saveUser(user);
        model.addAttribute("users", userService.getAllUsers());
        return "registration-done-valid-interface";
    }
}
