package com.ra.samples.springmvc.noboot.annotformatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.number.CurrencyStyleFormatter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("annotformatter")
public class AnnotatedFormatterTradeController {

    @Autowired
    private AnnotatedFormatterTradeService tradeService;


    @InitBinder
    private void customizeBinding (@PathVariable("tradeId") long tradeId, WebDataBinder binder) {
        AnnotatedFormatterTrade trade = tradeService.getTradeById(tradeId);
        if (trade == null) {
            return;
        }

        CurrencyStyleFormatter currencyFormatter = new CurrencyStyleFormatter();
        currencyFormatter.setCurrency(
                "Buy".equals(trade.getBuySell()) ? trade.getBuyCurrency() : trade
                        .getSellCurrency());
        binder.addCustomFormatter(currencyFormatter, "amount");
    }


    @RequestMapping("/{tradeId:\\d+}")
    public String handleTradeRequest (@PathVariable("tradeId") long tradeId, Model model) {
        AnnotatedFormatterTrade trade = tradeService.getTradeById(tradeId);
        if (trade == null) {
            model.addAttribute("msg", "No trade found");
            return "formatter/annotated/no-trade-page";
        }
        model.addAttribute("trade", trade);
        return "formatter/annotated/trade-page";
    }
}
