package com.ra.samples.springmvc.noboot.validintrface;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class UserServiceValidatorImpl implements UserServiceValidator {
    private Map<Long, UserValidatorModel> userMap = new HashMap<>();

    @Override
    public void saveUser(UserValidatorModel user) {
        if (user.getId() == null) {
            user.setId((long) (userMap.size() + 1));
        }
        userMap.put(user.getId(), user);

    }

    @Override
    public List<UserValidatorModel> getAllUsers() {
        return new ArrayList<>(userMap.values());
    }
}
