package com.ra.samples.springmvc.noboot.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/rest/users")
public class UserRestController {

    @Autowired
    private UserRestService userService;

    @RequestMapping(value = "register", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<MessageResult> handleJsonPostRequest (@RequestBody @Valid UserRest user, BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            var error = bindingResult.getFieldErrors().get(0);
            if("password".equals(error.getField())) {
                return
                        new ResponseEntity<>(
                                new MessageResult("ID-GP-001", "Password should not be empty"), HttpStatus.BAD_REQUEST);

            }
        }
       return new ResponseEntity<>(new MessageResult("", "Successfully created"), HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<UserRest> handleAllUserRequest () {
        return userService.getAllUsers();
    }
}
