package com.ra.samples.springmvc.noboot.newformatter;

public class NewFormatterCustomer {
    private Long id;
    private String name;
    private NewFormatterAddress address;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public NewFormatterAddress getAddress() {
        return address;
    }

    public void setAddress(NewFormatterAddress address) {
        this.address = address;
    }
}
