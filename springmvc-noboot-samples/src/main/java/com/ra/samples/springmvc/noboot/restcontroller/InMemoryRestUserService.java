package com.ra.samples.springmvc.noboot.restcontroller;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
public class InMemoryRestUserService implements UserRestService {

    private Map<Long, UserRest> userMap = new LinkedHashMap<>();

    @Override
    public void saveUser (UserRest user) {
        if (user.getId() == null) {
            user.setId((long) (userMap.size() + 1));
        }
        userMap.put(user.getId(), user);

    }

    @Override
    public List<UserRest> getAllUsers () {
        return new ArrayList<>(userMap.values());
    }
}
