package com.ra.samples.springmvc.noboot.controlleradvice;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("controlleradvice")
public class ControllerAdviceServicesController {

    @RequestMapping("/services/**")
    public String handleRequest() {
        return "advice/app-page";
    }
}
