package com.ra.samples.springmvc.noboot.uri;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/other/")
@ResponseBody
public class MyController2 {

    @RequestMapping("/c*/s*d/info")//score 2, length = 12, wildcards=2
    public String otherTest1(HttpServletRequest request) {
        return "from otherTest1(), request uri: " + request.getRequestURI();
    }

    @RequestMapping("/card/{type}/info")//score 1, length 12 (/card/#/info), wildcards=0
    public String otherTest2(@PathVariable String type, HttpServletRequest request) {
        return "from otherTest2(), request uri: " + request.getRequestURI() + "\n" +
                "type: " + type;
    }
}
