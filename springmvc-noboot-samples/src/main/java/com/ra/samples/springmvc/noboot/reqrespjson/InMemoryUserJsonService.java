package com.ra.samples.springmvc.noboot.reqrespjson;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
public class InMemoryUserJsonService implements UserJsonService {

    private Map<Long, UserJson> userMap = new LinkedHashMap<>();

    @Override
    public void saveUser (UserJson user) {
        if (user.getId() == null) {
            user.setId((long) (userMap.size() + 1));
        }
        userMap.put(user.getId(), user);

    }

    @Override
    public List<UserJson> getAllUsers () {
        return new ArrayList<>(userMap.values());
    }
}
