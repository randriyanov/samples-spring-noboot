package com.ra.samples.springmvc.noboot.reqrespxml;

import java.util.List;

public interface UserXmlService {

    void saveUser(UserXml user);

    List<UserXml> getAllUsers();
}
