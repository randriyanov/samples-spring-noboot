package com.ra.samples.springmvc.noboot.messagebeanvalid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
@RequestMapping("messagebeanvalid")
public class UserRegistrationMessageBeanValidationController {

    @Autowired
    private UserServiceMessageBeanValidation userService;

    @RequestMapping(method = RequestMethod.GET)
    public String handleGetRequest (Model model) {
        model.addAttribute("user", new UserMessageBeanValidation());
        return "user-registration-message";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String handlePostRequest (@Valid @ModelAttribute("user") UserMessageBeanValidation user,
                                     BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "user-registration-message";
        }

        userService.saveUser(user);
        return "registration-done-message";
    }
}
