package com.ra.samples.springmvc.noboot.reqrespjson;

import java.util.List;

public interface UserJsonService {
    void saveUser(UserJson user);

    List<UserJson> getAllUsers();
}
