package com.ra.samples.springmvc.noboot.restcontroller;

import java.util.List;

public interface UserRestService {

    void saveUser(UserRest user);

    List<UserRest> getAllUsers();
}
