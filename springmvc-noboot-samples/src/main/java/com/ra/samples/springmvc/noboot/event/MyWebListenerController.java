package com.ra.samples.springmvc.noboot.event;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/listener")
public class MyWebListenerController {

    @RequestMapping("/test")
    @ResponseBody
    public String handle () {
        return "test response from /test";
    }

    @RequestMapping("/test2")
    @ResponseBody
    public String handle2 () {
        return "test response from /test2";
    }
}
