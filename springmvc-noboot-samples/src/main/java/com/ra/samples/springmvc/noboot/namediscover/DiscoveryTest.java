package com.ra.samples.springmvc.noboot.namediscover;

import org.springframework.core.LocalVariableTableParameterNameDiscoverer;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Arrays;

public class DiscoveryTest {

    public void testMethod(String myParamName) {
    }

    public static void main(String[] args) throws NoSuchMethodException {
        Method m = DiscoveryTest.class.getDeclaredMethod("testMethod", String.class);
        for (Parameter parameter : m.getParameters()) {
            System.out.println(parameter.getName());
        }

        LocalVariableTableParameterNameDiscoverer d =
                new LocalVariableTableParameterNameDiscoverer();
        String[] pNames = d.getParameterNames(m);
        System.out.println(Arrays.toString(pNames));
    }
}
