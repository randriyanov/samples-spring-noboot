package com.ra.samples.springmvc.noboot.exceptionhandler;

public class UserNotLoggedInException extends Exception {

    public UserNotLoggedInException (String message) {
        super(message);
    }
}
