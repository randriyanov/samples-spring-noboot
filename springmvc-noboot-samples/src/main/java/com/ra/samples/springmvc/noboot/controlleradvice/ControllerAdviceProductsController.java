package com.ra.samples.springmvc.noboot.controlleradvice;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("controlleradvice")
public class ControllerAdviceProductsController {

    @RequestMapping("/products/**")
    public String handleRequest(){
        //todo:prepare page content
        return "advice/app-page";
    }
}
