[ContentNegotiationManager]

The ContentNegotiationManager is the central class to determine requested media types for a request. 
Its resolveMediaTypes() method does not actually resolve the media types itself but instead delegates 
to a list of configured ContentNegotiationStrategies for the current request

[HeaderContentNegotiationStrategy]
It reads the 'Accept' request header to resolve the MediaTypes.

[ServletPathExtensionContentNegotiationStrategy]
It resolves the file extension in the request path to a media type.
 It also uses ServletContext.getMimeType(String) to resolve file extensions.

By default 'xml' file extension is registered targeting MediaType.APPLICATION_XML. Depending on the related dependencies present in the classpath, more extensions (json, xml, rss, atom) are registered by default. Check out the source code of WebMvcConfigurationSupport#getDefaultMediaTypes()

[The Order of ContentNegotiationStrategies]
The strategies are applied in an order as seen in above output. 
On addition of more strategies the order will be applied as specified here, 
or we can also look at the source code of ContentNegotiationManagerFactoryBean#afterPropertiesSet() 
where the strategies are added in a particular order.


[RequestMappingHandlerMapping]
For RequestMappingHandlerMapping, ContentNegotiationManager#resolveMediaTypes() 
are matched against the value(s) specified in RequestMapping#produces or RequestMapping#headers 
elements of the handler methods till a match is selected.

[RequestMappingHandlerAdapter]
Handler's returned Object to HTTP Response body resolution
RequestMappingHandlerAdapter uses the media types returned by ContentNegotiationManager#resolveMediaTypes() to select a suitable HttpMessageConverter. The writeInternal() method of the selected HttpMessageConverter is then called with handler method's returned value to perform the conversion.

HTTP Request body to Handler method argument resolution
Based on the request 'Content-Type' header a suitable HttpMessageConverter is selected and then the readInternal() method of the selected HttpMessageConverter is called to convert request body to Java Object. RequestMappingHandlerAdapter delegates this process to an instance of HandlerMethodArgumentResolver. The selection logic is implemented in AbstractMessageConverterMethodArgumentResolver# readWithMessageConverters().



[ResourceHandlerRegistry]
It applies the ContentNegotiationStrategies to the static resources which include images, css files etc.

[ViewResolverRegistry]
It actually applies ContentNegotiationStrategies to ContentNegotiatingViewResolver which uses the media type returned by the ContentNegotiationStrategies to select a suitable View for a request during view rendering time.

[ExceptionHandlerExceptionResolver]
Similar to RequestMappingHandlerAdapter, ExceptionHandlerExceptionResolver uses ContentNegotiationStrategies to select a suitable HttpMessageConverter to prepare the response (from the method annotated with @ExceptionHandler).