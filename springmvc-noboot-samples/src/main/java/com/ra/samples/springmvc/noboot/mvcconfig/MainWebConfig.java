package com.ra.samples.springmvc.noboot.mvcconfig;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@Configuration
@ComponentScan(basePackages = "com.ra.samples.springmvc.noboot", excludeFilters =
@Filter(type = FilterType.REGEX, pattern = "com.ra.samples.*.*.appscope"))
public class MainWebConfig {

}
