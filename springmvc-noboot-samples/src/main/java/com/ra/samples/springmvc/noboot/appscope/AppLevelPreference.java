package com.ra.samples.springmvc.noboot.appscope;

import java.io.Serializable;

public class AppLevelPreference implements Serializable {
    private String background = "#fff";
    private String fontSize = "14px";

    public String getBackground() {
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public String getFontSize() {
        return fontSize;
    }

    public void setFontSize(String fontSize) {
        this.fontSize = fontSize;
    }
}
