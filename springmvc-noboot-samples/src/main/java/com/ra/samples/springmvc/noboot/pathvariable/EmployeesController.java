package com.ra.samples.springmvc.noboot.pathvariable;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * This will cause exception :
 * java.lang.IllegalStateException: Ambiguous handler methods ..
 */
@Controller
@RequestMapping("/employees")
public class EmployeesController {


    @RequestMapping("{id}")
    public String handleRequest(@PathVariable("id") String userId, Model model){
        model.addAttribute("msg", "employee id: "+userId);
        return "my-page";

    }

    @RequestMapping(method = RequestMethod.POST, path = "{employeeName}")
    public String handleRequest2 (@PathVariable("employeeName") String userName, Model model) {
        model.addAttribute("msg", "employee name : " + userName);
        return "my-page";
    }
}
