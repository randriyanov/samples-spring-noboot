package com.ra.samples.springmvc.noboot.mvcconfig;

import com.ra.samples.springmvc.noboot.extconverter.ReportConverter;
import com.ra.samples.springmvc.noboot.newformatter.AddressFormatter;
import com.ra.samples.springmvc.noboot.presessionatr.PreExistingSessionAttributesAdapter;
import com.ra.samples.springmvc.noboot.reqattribute.MyCounterInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import java.util.List;

//extends WebMvcConfigurerAdapter
@Configuration
public class MyViewConfig implements WebMvcConfigurer {

//    @Override
//    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
//        converters.add(new TheCustomConverter());
//    }

    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(new TheCustomConverter());
        converters.add(new ReportConverter());
    }

    @Override
    public void addFormatters (FormatterRegistry registry) {
        AddressFormatter addressFormatter = new AddressFormatter();
        addressFormatter.setStyle(AddressFormatter.Style.REGION);
        registry.addFormatter(addressFormatter);
    }

    @Override
    public void addInterceptors (InterceptorRegistry registry) {
        registry.addInterceptor(new MyCounterInterceptor());
        registry.addInterceptor(new PreExistingSessionAttributesAdapter());
    }

    @Bean
    public ViewResolver viewResolver() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");
        return viewResolver;
    }

//    @Override
//    public void configureContentNegotiation (ContentNegotiationConfigurer configurer) {
//        configurer.favorParameter(true);
//    }

    @Override
    public void addViewControllers (ViewControllerRegistry registry) {

        //mapping url to a view
        ViewControllerRegistration r = registry.addViewController("/handlermapping");
        r.setViewName("handlermapping-view");
        //setting status code
        r.setStatusCode(HttpStatus.GONE);

        //mapping another url to a status code
        registry.addStatusController("/handlermapping/test2", HttpStatus.SERVICE_UNAVAILABLE);
    }

}
