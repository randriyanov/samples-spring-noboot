package com.ra.samples.springmvc.noboot.negotiationbase;

import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.accept.ContentNegotiationManager;
import org.springframework.web.accept.ContentNegotiationStrategy;
import org.springframework.web.accept.ServletPathExtensionContentNegotiationStrategy;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("negotiationbase")
public class ViewAllResolversController {

    @Autowired
    ApplicationContext context;

    @RequestMapping("/")
    @ResponseBody
    public String handleRequest () {
        Map<String, ContentNegotiationStrategy> map =
                BeanFactoryUtils.beansOfTypeIncludingAncestors(
                        context, ContentNegotiationStrategy.class,
                        true, false);

        System.out.println("-- list of ContentNegotiationStrategies registered as beans --");
        map.forEach((k, v) -> System.out.printf("%s=%s%n",
                k, v.getClass().getSimpleName()));

        System.out.println("-- list of ContentNegotiationStrategies configured with ContentNegotiationManager --");
        ContentNegotiationManager m = (ContentNegotiationManager) map.get("mvcContentNegotiationManager");
        List<ContentNegotiationStrategy> strategies = m.getStrategies();
        strategies.forEach(s -> System.out.println(s.getClass().getName()));

        for (ContentNegotiationStrategy cns : m.getStrategies()) {
            if (cns instanceof ServletPathExtensionContentNegotiationStrategy) {
                List<String> list = ((ServletPathExtensionContentNegotiationStrategy) cns).getAllFileExtensions();
                System.out.println("-- file extensions configured with ServletPathExtensionContentNegotiationStrategy --");
                list.forEach(System.out::println);
            }
        }

        return "response";
    }
}
