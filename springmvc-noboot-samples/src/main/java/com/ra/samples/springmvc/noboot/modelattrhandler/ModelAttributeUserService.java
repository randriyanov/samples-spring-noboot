package com.ra.samples.springmvc.noboot.modelattrhandler;

public interface ModelAttributeUserService {

    ModelAttributeUser getUserById (long id);
}
