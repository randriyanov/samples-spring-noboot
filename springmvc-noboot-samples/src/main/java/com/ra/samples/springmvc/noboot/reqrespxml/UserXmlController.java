package com.ra.samples.springmvc.noboot.reqrespxml;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
@RequestMapping("userxml")
public class UserXmlController {

    @Autowired
    private UserXmlService userService;

    @RequestMapping(value = "register",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_XML_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public void handleXMLPostRequest (@RequestBody UserXml user) {
        System.out.println("saving user: " + user);
        userService.saveUser(user);
    }

    @RequestMapping(method = RequestMethod.GET,
            produces = MediaType.APPLICATION_XML_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ResponseBody
    public UserXmlList handleAllUserRequest () {
        UserXmlList list = new UserXmlList();
        list.setUsers(userService.getAllUsers());
        return list;
    }
}
