package com.ra.samples.springmvc.noboot.reqscope;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;


/**
 * The scope 'request' defines a single bean definition which lives within a single HTTP request.
 * That means for each HTTP request a new bean instance is created.
 * This scope is only valid in the context of a web-aware ApplicationContext.
 *
 * This scope might be very useful in using helper objects across multiple beans through out a
 * single request. This is particularly useful as compared to passing various parameters through
 * a long chain of methods calls which sometimes becomes unmaintainable
 * and it's difficult to add new parameters.
 */
@Component
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class EmployeeDetails {
    private Employee employee;
    private int lastYearSalary;

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Employee getEmployee() {
        return employee;
    }

    public int getLastYearSalary() {
        return lastYearSalary;
    }

    public void setLastYearSalary(int lastYearSalary) {
        this.lastYearSalary = lastYearSalary;
    }
}
