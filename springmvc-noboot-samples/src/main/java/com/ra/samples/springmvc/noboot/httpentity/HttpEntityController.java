package com.ra.samples.springmvc.noboot.httpentity;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Arrays;

//request body: User{name='joe', emailAddress='joe@example.com'}
//request headers {testHeader=[headerValue], Content-Type=[application/json], Content-Length=[50]}
@Controller
@RequestMapping("httpentity")
public class HttpEntityController {

    @RequestMapping
    public HttpEntity<String> handleRequest (HttpEntity<String> requestEntity) {
        System.out.println("request body: " + requestEntity.getBody());
        HttpHeaders headers = requestEntity.getHeaders();
        System.out.println("request headers " + headers);
        HttpEntity<String> responseEntity = new HttpEntity<String>("my response body");
        return responseEntity;
    }

    @RequestMapping("/user")
    public HttpEntity<String> handleUserRequest (HttpEntity<HttpEntityUser> requestEntity) {
        HttpEntityUser user = requestEntity.getBody();
        System.out.println("request body: " + user);
        System.out.println("request headers " + requestEntity.getHeaders());

        MultiValueMap<String, String> headers = new HttpHeaders();
        headers.put("Cache-Control", Arrays.asList("max-age=3600"));

        HttpEntity<String> responseEntity = new HttpEntity<>("my response body", headers);
        return responseEntity;
    }
}
