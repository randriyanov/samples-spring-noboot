package com.ra.samples.springmvc.noboot.httpsession;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;

/**
 * another option of working with low level Servlet API to maintain session attributes.
 * In this example we are going to use javax.servlet.http.HttpSession as a controller method argument.
 * Spring supports this argument and it is never null.
 * In the background, Spring uses HttpServletRequest#getSession()
 * (check out ServletRequestMethodArgumentResolver #resolveArgument() method)
 * which returns the session associated with the current request,
 * or if the request does not have a session, a new session object is created.
 */
@Controller
@ResponseBody
@RequestMapping("httpsession")
public class HttpSessionController {

    @RequestMapping(value = "/", produces = MediaType.TEXT_PLAIN_VALUE)
    public String handler(HttpSession httpSession) {
        String sessionKey = "firstAccessTime";
        Object time = httpSession.getAttribute(sessionKey);
        if (time == null) {
            time = LocalDateTime.now();
            httpSession.setAttribute(sessionKey, time);
        }
        return "first access time : " + time+"\nsession id: "+httpSession.getId();
    }
}
