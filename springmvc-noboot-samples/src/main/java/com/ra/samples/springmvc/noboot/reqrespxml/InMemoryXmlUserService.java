package com.ra.samples.springmvc.noboot.reqrespxml;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
public class InMemoryXmlUserService implements UserXmlService {

    private Map<Long, UserXml> userMap = new LinkedHashMap<>();

    @Override
    public void saveUser (UserXml user) {
        if (user.getId() == null) {
            user.setId((long) (userMap.size() + 1));
        }
        userMap.put(user.getId(), user);

    }

    @Override
    public List<UserXml> getAllUsers () {
        return new ArrayList<>(userMap.values());
    }
}
