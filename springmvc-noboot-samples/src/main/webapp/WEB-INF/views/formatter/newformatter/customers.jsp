<%@ page language="java"
         contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<html>
<body>
<h3>Customer List </h3>

<table style="width:100%">
    <c:forEach var="customer" items="${customerList}" varStatus="status">
        <tr>
            <td>
                <spring:eval expression="customer.id" />
            </td>
            <td>
                <spring:eval expression="customer.name" />
            </td>
            <td>
                <spring:eval expression="customer.address" />
            </td>
        </tr>
    </c:forEach>
</table>