package com.ra.samples.springmvc.noboot;

import com.ra.samples.springmvc.noboot.mvcconfig.MainWebConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@WebAppConfiguration
@ContextConfiguration(classes = MainWebConfig.class)
@ExtendWith(SpringExtension.class)
public class ModelUserControllerTest {

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup () {
        DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
        this.mockMvc = builder.build();
    }

    @Test
    public void testUserController () throws Exception {
        ResultMatcher ok = MockMvcResultMatchers.status()
                .isOk();

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get(
                "/model/users");
        this.mockMvc.perform(builder)
                .andExpect(ok);

        builder = MockMvcRequestBuilders.get(
                "/model/users/23");
        this.mockMvc.perform(builder)
                .andExpect(ok);

        builder = MockMvcRequestBuilders.get(
                "/model/users?querier=Joe");
        this.mockMvc.perform(builder)
                .andExpect(ok);

        builder = MockMvcRequestBuilders.get(
                "/model/users/23?querier=Joe");
        this.mockMvc.perform(builder)
                .andExpect(ok);

    }
}
