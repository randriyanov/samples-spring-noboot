1. Path Variable
2. Request Mapping
3. URI
4. Request Param
5. Binding Path Variable
6. Binding Object
7. Optional Handler
8. Enum Params
9. Composed
10. Option Header
11. Meta Annotations
12. Simple Form
13. Validation Form
14. ReqRespBody
15. ReqRespBody XML
16. Handler
17. Custom handler -> myconfig
18. ReqRespBody JSON
19. Extend Converter
20. Session Scoped Beans
21. HttpSession
22. Request Scope
23. Request Proxy
24. Application Scope
25. RequestAttribute
26. Rest Controller
27. Http entity
28. Request Entity
29. Controller Advice
30. Exception Handler