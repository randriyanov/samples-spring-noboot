/*
 * This file is generated by jOOQ.
 */
package springdb.noboot.samples.entity.tables.pojos;


import java.io.Serializable;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class BookToBookStore implements Serializable {

    private static final long serialVersionUID = 2019951299;

    private String  name;
    private Integer bookId;
    private Integer stock;

    public BookToBookStore() {}

    public BookToBookStore(BookToBookStore value) {
        this.name = value.name;
        this.bookId = value.bookId;
        this.stock = value.stock;
    }

    public BookToBookStore(
        String  name,
        Integer bookId,
        Integer stock
    ) {
        this.name = name;
        this.bookId = bookId;
        this.stock = stock;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getBookId() {
        return this.bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public Integer getStock() {
        return this.stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("BookToBookStore (");

        sb.append(name);
        sb.append(", ").append(bookId);
        sb.append(", ").append(stock);

        sb.append(")");
        return sb.toString();
    }
}
