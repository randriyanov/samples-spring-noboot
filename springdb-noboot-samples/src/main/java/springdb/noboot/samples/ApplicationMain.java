package springdb.noboot.samples;


import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.impl.DefaultDSLContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import springdb.noboot.samples.entity.tables.pojos.Author;
import springdb.noboot.samples.entity.tables.records.AuthorRecord;

import static springdb.noboot.samples.entity.Tables.AUTHOR;
import static springdb.noboot.samples.entity.Tables.BOOK;


@ComponentScan("springdb.noboot.samples")
public class ApplicationMain {

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationMain.class);
        //ConnectionProvider provider = context.getBean(ConnectionProvider.class);
        //Stream.of(context.getBeanDefinitionNames()).forEach(System.out::println);

        DSLContext dslContext = context.getBean(DefaultDSLContext.class);
        Result<Record> result = dslContext.select().from(AUTHOR).fetch();
        AuthorRecord authorRecord =  dslContext.fetchOne(AUTHOR, AUTHOR.ID.eq(1));
        Author author = new Author();
        author.setId(authorRecord.getId());

        Result<?> joinRecord = dslContext.select(
                AUTHOR.FIRST_NAME,
                AUTHOR.LAST_NAME,
                BOOK.ID,
                BOOK.TITLE
        )
                .from(AUTHOR)
                .join(BOOK)
                .on(AUTHOR.ID.eq(BOOK.AUTHOR_ID))
                .orderBy(BOOK.ID.asc())
                .fetch();
    }
}
