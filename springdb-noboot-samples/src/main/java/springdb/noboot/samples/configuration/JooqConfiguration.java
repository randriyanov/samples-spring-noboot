package springdb.noboot.samples.configuration;

import org.jooq.ConnectionProvider;
import org.jooq.SQLDialect;
import org.jooq.impl.DataSourceConnectionProvider;
import org.jooq.impl.DefaultConfiguration;
import org.jooq.impl.DefaultDSLContext;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;

import javax.sql.DataSource;

@Configuration
public class JooqConfiguration {

    @Bean
    public ConnectionProvider dataSourceConnectionProvider(DataSource dataSource) {
        return new DataSourceConnectionProvider(dataSource);
    }

    @Bean
    public TransactionAwareDataSourceProxy transactionAwareDataSourceProxy(DataSource dataSource) {
        return new TransactionAwareDataSourceProxy(dataSource);
    }

    @Bean
    public DefaultConfiguration postgreConfig(ConnectionProvider dataSourceConnectionProvider) {
        var defConfig = new DefaultConfiguration();
        defConfig.setSQLDialect(SQLDialect.POSTGRES);
        defConfig.setConnectionProvider(dataSourceConnectionProvider);
        return defConfig;
    }

    @Bean
    public DefaultDSLContext defaultDSLContext(DefaultConfiguration configuration) {
        return new DefaultDSLContext(configuration);
    }
}
