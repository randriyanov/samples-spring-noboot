package springdb.noboot.samples.configuration;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;

import javax.sql.DataSource;

@Configuration
public class JdbcConfiguration {

    @Bean(destroyMethod = "close") // bean life cycle
    public BasicDataSource dataSource() {
        var datSourse = new BasicDataSource();
        datSourse.setUrl("jdbc:postgresql://localhost:5432/postgres");
        datSourse.setPassword("postgres");
        datSourse.setUsername("postgres");
        datSourse.setDriverClassName("org.postgresql.Driver");
        return datSourse;
    }

    @Bean
    public DataSourceTransactionManager dataSourceTransactionManager(DataSource dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean
    public TransactionAwareDataSourceProxy transactionAwareDataSourceProxy(DataSource dataSource) {
        return new TransactionAwareDataSourceProxy(dataSource);
    }
}
