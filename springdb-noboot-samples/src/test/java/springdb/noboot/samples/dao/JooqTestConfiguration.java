package springdb.noboot.samples.dao;

import org.apache.commons.dbcp2.BasicDataSource;
import org.jooq.ConnectionProvider;
import org.jooq.SQLDialect;
import org.jooq.impl.DefaultConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
@ComponentScan("springdb.noboot.samples.configuration")
public class JooqTestConfiguration {

    @Bean
    @Primary
    public DefaultConfiguration postgreConfig(ConnectionProvider dataSourceConnectionProvider) {
        var defConfig = new DefaultConfiguration();
        defConfig.setSQLDialect(SQLDialect.H2);
        defConfig.setConnectionProvider(dataSourceConnectionProvider);
        return defConfig;
    }

    @Primary
    @Bean // bean life cycle
    public BasicDataSource dataSource() {
        var datSourse = new BasicDataSource();
        datSourse.setUrl("jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;DATABASE_TO_UPPER=false;");

        datSourse.setDriverClassName("org.h2.Driver");
        return datSourse;
    }
}
