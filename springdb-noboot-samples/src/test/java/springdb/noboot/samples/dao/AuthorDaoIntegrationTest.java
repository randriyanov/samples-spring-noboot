package springdb.noboot.samples.dao;

import org.jooq.DSLContext;
import org.jooq.Meta;
import org.jooq.Record;
import org.jooq.Result;
import org.jooq.Row;
import org.jooq.impl.DSL;
import org.jooq.impl.DefaultConfiguration;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import springdb.noboot.samples.entity.tables.daos.AuthorDao;
import springdb.noboot.samples.entity.tables.pojos.Author;


import java.util.List;

import static springdb.noboot.samples.entity.Tables.AUTHOR;

@ContextConfiguration(classes = {JooqTestConfiguration.class, AuthorDao.class})
@ExtendWith({SpringExtension.class})
@Sql({"/db-scripts.sql"})
public class AuthorDaoIntegrationTest {

    @Autowired
    private AuthorDao authorDao;

    @Test
    public void test() {
        List<Author> result = authorDao.fetchById(1);
        //Result<Record> result = dsl.fetch(sql);
        System.out.println(result);
    }

}
