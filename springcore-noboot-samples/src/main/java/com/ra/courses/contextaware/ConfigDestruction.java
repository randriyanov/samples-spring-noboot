package com.ra.courses.contextaware;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigDestruction {

    @Bean
    public DestructiveBeanWithInterface destructiveBeanWithInterface() {
        return new DestructiveBeanWithInterface();
    }
}
