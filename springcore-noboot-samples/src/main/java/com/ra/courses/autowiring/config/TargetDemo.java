package com.ra.courses.autowiring.config;

import com.ra.courses.autowiring.sandbox.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class TargetDemo {

	@Autowired
	private Foo foo;

	@Configuration
	static class TargetConfig {

		@Bean
		public Foo fooImplOne() {
			return new FooImplOne();
		}

		@Bean
		public Foo fooImplTwo() {
			return new FooImplTwo();
		}

		@Bean
		public Bar bar() {
			return new Bar();
		}

		@Bean
		public TrickyTarget trickyTarget() {
			return new TrickyTarget();
		}
	}

	public static void main(String[] args) {
		GenericApplicationContext ctx = new AnnotationConfigApplicationContext(TargetConfig.class);
		TrickyTarget t = ctx.getBean(TrickyTarget.class);
		ctx.close();
	}
}
