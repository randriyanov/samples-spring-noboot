package com.ra.courses.autowiring.annotated;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan("com.ra.courses.autowiring.annotated")
@Configuration
public class AnnotatedDependsOnDemo {

	public static void main(String... args) {
		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(AnnotatedDependsOnDemo.class);
		Singer johnMayer = ctx.getBean("johnMayer", Singer.class);
		johnMayer.sing();
		ctx.close();
	}

}
