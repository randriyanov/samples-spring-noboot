package com.ra.courses.aliases;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.core.annotation.AnnotationAttributes;

@Configuration
public class ExampleAliasFor {

    public static void main(String[] args) {
        AnnotationAttributes aa = AnnotatedElementUtils
                .getMergedAnnotationAttributes(MyObject1.class, AccessRole.class);
        System.out.println("Attributes of AccessRole used on MyObject1: " + aa);
    }
}