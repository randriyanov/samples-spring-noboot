package com.ra.courses.aliases;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface AccessRole2 {

    @AliasFor("accessType")
    String value() default "visitor";

    @AliasFor("value")
    String accessType() default "admin";

    String module() default "gui";
}
