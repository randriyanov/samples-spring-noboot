package com.ra.courses.example;

import java.util.List;

public class Student {
    private List<Integer> marks;

    public List<Integer> getMarks() {
        return marks;
    }

    public void setMarks(List<Integer> marks) {
        this.marks = marks;
    }

    public Student(List<Integer> marks) {
        this.marks = marks;
    }

    public int getMarksQty() {
        return marks.size();
    }
}
