package com.ra.courses.helloworld.common;

/**
 * Created by iuliana.cosmina on 4/2/17.
 */
public interface Singer {
	void sing();
}
