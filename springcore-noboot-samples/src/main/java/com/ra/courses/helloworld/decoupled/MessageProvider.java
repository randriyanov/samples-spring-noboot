package com.ra.courses.helloworld.decoupled;

public interface MessageProvider {
    String getMessage();
}
