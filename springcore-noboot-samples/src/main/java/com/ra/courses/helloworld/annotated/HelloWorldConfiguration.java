package com.ra.courses.helloworld.annotated;

import com.ra.courses.helloworld.decoupled.HelloWorldMessageProvider;
import com.ra.courses.helloworld.decoupled.MessageProvider;
import com.ra.courses.helloworld.decoupled.MessageRenderer;
import com.ra.courses.helloworld.decoupled.StandardOutMessageRenderer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HelloWorldConfiguration {

    @Bean
    public MessageRenderer renderer(MessageProvider provider) {
        MessageRenderer renderer = new StandardOutMessageRenderer();
        renderer.setMessageProvider(provider);
        return renderer;
    }

    @Bean
    public MessageProvider provider() {
        return new HelloWorldMessageProvider();
    }


}
