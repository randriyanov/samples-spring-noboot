package com.ra.courses.helloworld.common;

public interface GiftedSinger extends Singer {
	void play(Guitar guitar);
}
