package com.ra.courses.method.annotated;

public interface DemoBean {
    Singer getMySinger();
    void doSomething();
}
