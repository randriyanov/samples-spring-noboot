package com.ra.courses.method;

public interface DemoBean {
    Singer getMySinger();
    void doSomething();
}
