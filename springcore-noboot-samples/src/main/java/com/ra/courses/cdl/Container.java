package com.ra.courses.cdl;

public interface Container {
    Object getDependency(String key);
}
