package com.ra.courses.cdl;

public interface ManagedComponent {
    void performLookup(Container container);  
}
