package com.ra.courses.qualifier;

import com.ra.courses.autowiring.config.TargetDemo;
import com.ra.courses.autowiring.sandbox.TrickyTarget;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericApplicationContext;

public class Main {
    public static void main(String[] args) {
        GenericApplicationContext ctx = new AnnotationConfigApplicationContext(StudentContext.class);
        StudentService t = ctx.getBean(StudentService.class);
        t.printStudentName();
        ctx.close();
    }
}
