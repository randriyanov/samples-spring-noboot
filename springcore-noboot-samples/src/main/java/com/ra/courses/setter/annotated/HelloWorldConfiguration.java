package com.ra.courses.setter.annotated;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@ComponentScan(basePackages = {"com.ra.courses.setter.annotated"})
@Configuration
public class HelloWorldConfiguration {

}
