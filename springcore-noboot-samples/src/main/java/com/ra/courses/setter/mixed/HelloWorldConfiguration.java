package com.ra.courses.setter.mixed;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;


@ImportResource(locations = {"classpath:spring/setter/app-context.xml"})
@Configuration
public class HelloWorldConfiguration {

}
