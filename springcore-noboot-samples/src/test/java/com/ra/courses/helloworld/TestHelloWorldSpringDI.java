package com.ra.courses.helloworld;

import com.ra.courses.helloworld.decoupled.MessageRenderer;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static org.junit.jupiter.api.Assertions.assertNotNull;


public class TestHelloWorldSpringDI {

	@Test
	public void testHello() {
		ApplicationContext ctx = new ClassPathXmlApplicationContext
				("spring/app-context.xml");
		MessageRenderer mr = ctx.getBean("renderer", MessageRenderer.class);
		assertNotNull(mr);
	}
}
