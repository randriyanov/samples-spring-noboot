package com.ra.courses.helloworld;

import com.ra.courses.helloworld.annotated.HelloWorldConfiguration;
import com.ra.courses.helloworld.decoupled.MessageRenderer;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.assertNotNull;


public class TestHelloWorldSpringAnnotated {

	@Test
	public void testHello() {
		ApplicationContext ctx = new AnnotationConfigApplicationContext
				(HelloWorldConfiguration.class);
		MessageRenderer mr = ctx.getBean("renderer", MessageRenderer.class);
		assertNotNull(mr);
	}
}
